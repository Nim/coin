from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError

from netfields import InetAddressField, NetManager

import ldapdb.models
from ldapdb.models.fields import CharField, ListField

from coin.mixins import CoinLdapSyncMixin
from coin.configuration.models import Configuration
from coin import validation, utils

# Implementationo of a simple Radius backend for DSL, stored in LDAP.

# Settings
# - LDAP branch

class RadiusGroup(models.Model):
    """This implements a notion of "Radius group", which is mostly useful to
    support multiple backhaul operators (i.e. each object provides
    settings for a specific backhaul operator).

    For now, it only changes the realm of the login stored in LDAP, but it
    might also be used in the future to implement different Radius profile
    for each group (different L2TP server, etc).  One possible usage could
    be to load-balance users (possibly on the same backhaul
    infrastracture) to different L2TP servers by placing them in different
    Radius groups.
    """
    name = models.CharField(max_length=256, verbose_name="nom du groupe")
    realm = models.CharField(max_length=50,
                             verbose_name="realm radius",
                             help_text='Exemple : « fdn.nerim »')
    suffix = models.CharField(max_length=50, blank=True,
                              verbose_name="suffixe",
                              help_text='Suffixe optionnel, ajouté après le login, comme un « sous-realm ». Exemple : "%gnd"')
    comment = models.CharField(max_length=256, blank=True,
                               verbose_name="commentaire")

    def __unicode__(self):
        return f"{self.name} ({self.suffix}@{self.realm})"

    class Meta:
        verbose_name = "groupe radius"
        verbose_name_plural = "groupes radius"
        app_label = "dsl_ldap"


class DSLConfiguration(CoinLdapSyncMixin, Configuration):
    """TODO: make sure that the (full) login never changes, because it's a
    LDAP primary key.  Or at least delete the old object and create a new
    one when the login changes.
    """
    url_namespace = "dsl_ldap"
    phone_number = models.CharField(max_length=20,
                                    verbose_name="numéro de téléphone",
                                    help_text="numéro de téléphone associé à la ligne xDSL")
    activated = models.BooleanField(default=False, verbose_name='actif')
    radius_group = models.ForeignKey(RadiusGroup, verbose_name="groupe radius",
                                     help_text="Groupe (ex : backhaul) à utiliser",
                                     on_delete=models.CASCADE)
    # TODO: introduce forbidden characters (and read RFC2865)
    login = models.CharField(max_length=50, unique=True, blank=True,
                             verbose_name="login",
                             help_text="Laisser vide pour auto-générer.")
    password = models.CharField(max_length=256, blank=True,
                                verbose_name="mot de passe",
                                help_text="Est stocké en clair ! Auto-généré si laissé vide.")

    objects = NetManager()


    def full_login(self):
        """Login with realm"""
        return "{}{}@{}".format(self.login, self.radius_group.suffix,
                                self.radius_group.realm)

    def clean(self):
        # Generate DSL login, of the form "login-dslX".
        if not self.login:
            username = self.offersubscription.member.username
            dsl_lines = DSLConfiguration.objects.filter(offersubscription__member__username=username)
            # This is the list of existing DSL logins for this user.
            logins = [dsl.login for dsl in dsl_lines]
            # 100 DSL lines ought to be enough for anybody.
            for login in [f"{username}-dsl{k}" for k in range(1, 101)]:
                if login not in logins:
                    self.login = login
                    break
            # We may have failed.
            if not self.login:
                ValidationError("Impossible de générer un login DSL")
        # Generate password: 8 lowercase letters.  We don't really care
        # about security, since 1/ we store it in cleartext 2/ to
        # bruteforce it, you must have a DSL line on the same backhaul
        # infrastructure.  On the other hand, it must be easy to copy it
        # by hand to configure a modem.
        if not self.password:
            self.password = utils.generate_weak_password(8)

    # This method is part of the general configuration interface.
    def subnet_event(self):
        self.sync_to_ldap(False)

    def get_subnets(self, version):
        subnets = self.ip_subnet.all()
        return [subnet for subnet in subnets if subnet.inet.version == version]

    def sync_to_ldap(self, creation, *args, **kwargs):
        if creation:
            config = LdapDSLConfig()
        else:
            config = LdapDSLConfig.objects.get(pk=self.full_login())
        config.login = self.full_login()
        config.cleartext_password = self.password
        config.password = utils.ldap_hash(self.password)
        config.active = 'yes' if self.activated else 'no'
        v4_subnets = self.get_subnets(4)
        v6_subnets = self.get_subnets(6)
        # Instead of having an explicit IPv4 endpoint in the model, we
        # simply take the first IPv4 subnet (which we assume is a /32)
        if len(v4_subnets) > 0:
            config.ipv4_endpoint = str(v4_subnets[0].inet.ip)
        else:
            config.ipv4_endpoint = None
        config.ranges_v4 = [str(s) for s in v4_subnets]
        if len(v6_subnets) > 0:
            config.ranges_v6 = [str(s) for s in v6_subnets]
        else:
            # This field is multi-valued, but mandatory... Hack hack hack.
            config.ranges_v6 = ["fd20:fd79:5eb6:5cc5::/64"]
        config.save()

    def delete_from_ldap(self):
        LdapDSLConfig.objects.get(pk=self.full_login()).delete()

    def __unicode__(self):
        return self.full_login()

    class Meta:
        verbose_name = "Ligne xDSL"
        verbose_name_plural = "Lignes xDSL"
        app_label = "dsl_ldap"


class LdapDSLConfig(ldapdb.models.Model):
    base_dn = settings.DSL_CONF_BASE_DN # "ou=radius,o=ILLYSE,l=Villeurbanne,st=RHA,c=FR"
    object_classes = ['top', 'radiusObjectProfile', 'radiusprofile', 'ipHost']

    login = CharField(db_column='cn', primary_key=True, max_length=255)
    password = CharField(db_column='userPassword', max_length=255)
    cleartext_password = CharField(db_column='description', max_length=255)
    active = CharField(db_column='dialupAccess', max_length=3)
    ipv4_endpoint = CharField(db_column='radiusFramedIPAddress', max_length=16)
    ranges_v4 = ListField(db_column='radiusFramedRoute')
    # This field is multi-valued, but mandatory...
    ranges_v6 = ListField(db_column='ipHostNumber')

    def __unicode__(self):
        return self.login

    class Meta:
        managed = False  # Indique à South de ne pas gérer le model LdapUser
        app_label = "dsl_ldap"

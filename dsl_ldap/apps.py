from django.apps import AppConfig
import coin.apps


class DSLLDAPConfig(AppConfig, coin.apps.AppURLs):
    name = 'dsl_ldap'
    verbose_name = "xDSL (LDAP)"

    exported_urlpatterns = [('dsl_ldap', 'dsl_ldap.urls')]

    admin_menu_addons = {
        'configs': [
            ("xDSL (via LDAP)", "dsl_ldap/dslconfiguration"),
        ],
    }

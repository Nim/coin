from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('housing', '0004_auto_20200717_1733'),
    ]

    operations = [
        migrations.AlterField(
            model_name='housingconfiguration',
            name='vlan',
            field=models.IntegerField(default=0, null=True, verbose_name='vlan id'),
        ),
    ]

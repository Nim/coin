from django import template
from ipaddress import ip_network


register = template.Library()

@register.filter
def prettify(subnet):
    """Prettify an IPv4 subnet by remove the subnet length when it is equal to /32
    """
    if hasattr(subnet, "inet") and isinstance(subnet.inet, ip_network):
        subnet = subnet.inet
    if isinstance(subnet, ip_network):
        if subnet.version == 4 and subnet.prefixlen == 32:
            return str(subnet.ip)
        elif subnet.version == 6 and subnet.prefixlen == 128:
            return str(subnet.ip)
        else:
            return str(subnet)
    return subnet

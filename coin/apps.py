from os.path import basename

import six
from django.apps import apps

from .utils_light import rstrip_str


class AppURLsMeta(type):
    def __init__(cls, name, bases, data):
        if len(bases) > 1: # execute only on leaf class
            exported_urlpatterns = data.pop('exported_urlpatterns', None)

            if exported_urlpatterns:
                cls.exported_urlpatterns = exported_urlpatterns
            else:
                # Default : sets
                #   exported_urlpatterns = [(<app_name>, <app_url_module>)]
                current_path = '.' + rstrip_str(rstrip_str(basename(__file__), '.pyc'), '.py')
                url_module = rstrip_str(cls.__module__, current_path) + '.urls'
                cls.exported_urlpatterns = [(data['name'], url_module)]

            cls.urlprefix = data.pop('urlprefix', None)


class AppURLs(metaclass=AppURLsMeta):
    """ App Mixxin to allow an application to expose pluggable urls

    That's to say, URLs which will be added automatically to the projet
    urlpatterns.

    You can just make your app inherit from AppURLs, yous app urls.py will be
    picked and wired on project urlpatterns, using the app name as prefix.

    You can also customize which urlpatterns your app exposes by setting the
    `exported_urlpattens` on your AppConfig class as list of `<prefix>,<urlpatterns>`

    E.g:

        class MyAppConfig(AppConfig, coin.apps.AppURLS):
            name = 'my_app'
            exported_urlpatterns = [('my_app', 'myapp.cool_urls')]
    """
    pass

from django.template import Library

register = Library()

@register.filter
def provision_is_managed_via_hook(self):
    return self.provision_is_managed_via_hook()

@register.filter
def state_is_managed_via_hook(self):
    return self.state_is_managed_via_hook()

@register.filter
def state_icon_display(self):
    return self.get_state_icon_display()

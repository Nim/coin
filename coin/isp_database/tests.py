from django.contrib.auth.models import UserManager
from django.test import TestCase

# Create your tests here.

from coin.members.models import Member
from coin.isp_database.templatetags.isptags import pretty_iban
from .models import ChatRoom, ISPInfo

class TestPrettifiers(TestCase):
    def test_pretty_iban(self):
        """ Prints pretty readable IBAN

        Takes the IBAN in compact form and displays it according to the display spec
        See http://en.wikipedia.org/wiki/International_Bank_Account_Number#Practicalities
        """
        self.assertEqual(pretty_iban('DEkkBBBBBBBBCCCCCCCCCC'),
                         'DEkk BBBB BBBB CCCC CCCC CC')
        self.assertEqual(pretty_iban('ADkkBBBBSSSSCCCCCCCCCCCC'),
                         'ADkk BBBB SSSS CCCC CCCC CCCC')
        self.assertEqual(pretty_iban(''), '')

class TestContactPage(TestCase):
    def setUp(self):
        # Could be replaced by a force_login when we will be at Django 1.9
        Member.objects.create_user('user', password='password')
        self.client.login(username='user', password='password')

    def test_chat_view(self):
        isp = ISPInfo.objects.create(name='test', email='foo@example.com', )

        # Without chatroom
        response = self.client.get('/members/contact/')
        self.assertEqual(response.status_code, 200)

        # With chatroom
        ChatRoom.objects.create(
            isp=isp, url='irc://irc.example.com/#chan')

        response = self.client.get('/members/contact/')
        self.assertEqual(response.status_code, 200)

from .settings_base import *

# Surcharge les paramètres en utilisant le fichier settings_local.py
try:
    from settings_local import *
except ImportError:
    pass

TEMPLATES[0]['DIRS'] = EXTRA_TEMPLATE_DIRS + TEMPLATES[0]['DIRS']
INSTALLED_APPS = INSTALLED_APPS + EXTRA_INSTALLED_APPS

#TEMPLATES = {
#    'BACKEND': 'django.template.backends.django.DjangoTemplates',
#    'DIRS': TEMPLATE_DIRS,
#    'APP_DIRS': True,
#    'OPTIONS': {
#        'context_processors': TEMPLATE_CONTEXT_PROCESSORS
#    },
#}

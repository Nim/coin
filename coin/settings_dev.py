from .settings_base import *

DATABASES = {
    # Database hosted on vagant test box
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'coin',
        'USER': 'coin',
        'PASSWORD': 'kouaingkouaing',
        'HOST': 'db',
        'PORT': '5432',
    },
}


EXTRA_INSTALLED_APPS = (
    'hardware_provisioning',
    'maillists',
    'vpn',
    'vps',
    'housing',
)

# Surcharge les paramètres en utilisant le fichier settings_local.py
try:
    from settings_local import *
except ImportError:
    pass

TEMPLATES[0]['DIRS'] = EXTRA_TEMPLATE_DIRS + TEMPLATES[0]['DIRS']
INSTALLED_APPS = INSTALLED_APPS + EXTRA_INSTALLED_APPS

from django.db import migrations, models
import django.contrib.auth.models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0024_auto_20201203_1852'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='membershipfee',
            name='member',
        ),
        migrations.AlterModelManagers(
            name='member',
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.DeleteModel(
            name='MembershipFee',
        ),
    ]

from autocomplete_light import shortcuts as al
from .models import Member

# This will generate a MemberAutocomplete class
al.register(Member,
                            # Just like in ModelAdmin.search_fields
                            search_fields=[
                                '^first_name', '^last_name', 'organization_name',
                                '^username', '^nickname'],
                            attrs={
                                # This will set the input placeholder attribute:
                                'placeholder': 'Nom/Prénom/Pseudo (min 3 caractères)',
                                # Nombre minimum de caractères à saisir avant de compléter.
                                # Fixé à 3 pour ne pas qu'on puisse avoir accès à la liste de tous les membres facilement quand on n'est pas superuser.
                                'data-autocomplete-minimum-characters': 3,
                            },
)

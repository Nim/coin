"""
moved stuff required in coin/apps.py from coin/utils.py
as coin/utils.py imports Site which requires apps to be ready.
"""

def rstrip_str(s, suffix):
    """Return a copy of the string [s] with the string [suffix] removed from
    the end (if [s] ends with [suffix], otherwise return s)."""
    if s.endswith(suffix):
        return s[:-len(suffix)]
    else:
        return s



from django.apps import AppConfig
import coin.apps


class VPSConfig(AppConfig, coin.apps.AppURLs):
    name = 'vps'
    verbose_name = "Gestion d'accès VPS"

    exported_urlpatterns = [('vps', 'vps.urls')]

    admin_menu_addons = {
        'configs': [
            ("Serveur VPS", "vps/vpsconfiguration"),
        ],
        'infra': [
            ("OS pour VPS", "vps/vpsoperatingsystem"),
        ]
    }

from django.contrib import admin

from coin.configuration.admin import ChildConfigurationAdmin, ChildConfigurationAdminInline
from coin.offers.admin import ChildOfferSubscriptionRequestAdmin
from coin.utils import delete_selected

from .models import VPNConfiguration, VPNSubscriptionRequest


class VPNConfigurationInline(ChildConfigurationAdminInline):
    model = VPNConfiguration
    exclude = ('password',)
    readonly_fields = ['configuration_ptr', 'login']
    specific_fields = ["login", "crypto_link"]


class VPNConfigurationAdmin(ChildConfigurationAdmin):
    base_model = VPNConfiguration
    list_display = ('__unicode__', 'offersubscription', 'get_state_icon_display',
                    'ipv4_endpoint', 'ipv6_endpoint', 'comment')
    search_fields = ('login', 'comment',
                     # TODO: searching on member directly doesn't work
                     'offersubscription__member__first_name',
                     'offersubscription__member__last_name',
                     'offersubscription__member__email')
    actions = (delete_selected,)
    exclude = ("password",)
    inline = VPNConfigurationInline
    specific_fields = ["login", "crypto_link"]

class VPNSubscriptionRequestAdmin(ChildOfferSubscriptionRequestAdmin):

    base_model = VPNSubscriptionRequest


admin.site.register(VPNConfiguration, VPNConfigurationAdmin)

from django.apps import AppConfig
import coin.apps


class VPNConfig(AppConfig, coin.apps.AppURLs):
    name = 'vpn'
    verbose_name = "Tunnels VPN"

    exported_urlpatterns = [('vpn', 'vpn.urls')]

    admin_menu_addons = {
        'configs': [
            ("Tunnel VPN", "vpn/vpnconfiguration"),
        ]
    }
